﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    protected Rigidbody2D rb;
    protected Animator anim;

    public int fruits;

    [Header("Move info")]
    [SerializeField] protected float moveSpeed = 7f;
    [SerializeField] protected float jumpForce = 14f;
    [SerializeField] protected Vector2 wallJumpDirection;

    protected bool doubleJump = true;
    protected bool canMove;
    protected float movingInput;

    [SerializeField] protected float bufferJumpTime;
    protected float bufferJumpCounter;

    [SerializeField] protected float cayoteJumpTime;
    protected float cayoteJumpCounter;
    protected bool canHaveCayoteJump;

    [Header("Knockback info")]
    [SerializeField] protected Vector2 knockbackDirection;
    [SerializeField] protected float knockbackTime;
    [SerializeField] protected float knockbackProtectionTime;

    protected bool isKnocked;
    protected bool canBeKnocked = true;

    [Header("Collision info")]
    [SerializeField] protected LayerMask whatIsGround;
    [SerializeField] protected LayerMask whatIsWall;
    [SerializeField] protected float groundCheckDistance;
    [SerializeField] protected float wallCheckDistance;
    [SerializeField] protected Transform enemyCheck;
    [SerializeField] protected float enemyCheckRadius;

    protected bool isGrounded;
    protected bool isWallDetected;
    protected bool canWallSlide;
    protected bool isWallSliding;

    protected bool facingRight = true;
    protected int facingDirection = 1;

    protected void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }

    protected void Update()
    {
        AnimationControllers();

        if (isKnocked) return;

        FlipController();
        CollisionChecks();
        InputChecks();
        CheckForEnemy();

        bufferJumpCounter -= Time.deltaTime;
        cayoteJumpCounter -= Time.deltaTime;

        if (isGrounded)
        {
            doubleJump = true;
            canMove = true;

            if (bufferJumpCounter > 0)
            {
                bufferJumpCounter = -1;
                Jump();
            }
            canHaveCayoteJump = true;
        }
        else
        {
            if (canHaveCayoteJump)
            {
                canHaveCayoteJump = false;
                cayoteJumpCounter = cayoteJumpTime;
            }
        }

        if (canWallSlide)
        {
            isWallSliding = true;
            rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y * 0.1f);
        }

        Move();
    }

    protected void CheckForEnemy()
    {
        //Damage quái
        Collider2D[] hitedColliders = Physics2D.OverlapCircleAll(enemyCheck.position, enemyCheckRadius);

        foreach (var enemy in hitedColliders)
        {
            if (enemy.GetComponent<Enemy>() != null)
            {
                Enemy newEnemy = enemy.GetComponent<Enemy>();

                if (newEnemy.invincible) return;

                if (rb.velocity.y < 0)
                {
                    newEnemy.Damage();
                    Jump();
                }
            }
        }
    }

    private void AnimationControllers()
    {
        bool isMoving = rb.velocity.x != 0;

        anim.SetBool("isKnocked", isKnocked);
        anim.SetBool("isMoving", isMoving);
        anim.SetBool("isGrounded", isGrounded);
        anim.SetBool("isWallSliding", isWallSliding);
        anim.SetBool("isWallDetected", isWallDetected);
        anim.SetFloat("yVelocity", rb.velocity.y);
    }

    protected void InputChecks()
    {
        movingInput = Input.GetAxisRaw("Horizontal");

        if (Input.GetAxisRaw("Vertical") < 0)
        {
            canWallSlide = false;
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            JumpButton();
        }
    }

    protected void JumpButton()
    {
        if (!isGrounded)
        {
            bufferJumpCounter = bufferJumpTime;
        }

        if (isWallSliding)
        {
            WallJump();
            doubleJump = true;
        }
        else if (isGrounded || cayoteJumpCounter > 0)
        {
            Jump();
        }
        else if (doubleJump)
        {
            canMove = true;
            doubleJump = false;
            Jump();
        }

        canWallSlide = false;
    }

    public void Knockback(Transform damageTransform)
    {
        if (!canBeKnocked) return;

        if (GameManager.instance.difficulty > 1)
        {
            PlayerManager.instance.fruits--;
            if (PlayerManager.instance.fruits < 0)
            {
                Destroy(gameObject);
            }
        }

        PlayerManager.instance.ScreenShake(-facingDirection);
        isKnocked = true;
        canBeKnocked = false;

        #region xác định hướng cho animation hit
        int hDirection = 0;
        if (transform.position.x > damageTransform.position.x)
        {
            hDirection = 1;
        }
        else if (transform.position.x < damageTransform.position.x)
        {
            hDirection = -1;
        }
        #endregion

        rb.velocity = new Vector2 (knockbackDirection.x * hDirection, knockbackDirection.y);

        Invoke("CancelKnockback", knockbackTime);
        Invoke("AllowKnockback", knockbackProtectionTime);
    }

    protected void CancelKnockback()
    {
        isKnocked = false;
    }

    protected void AllowKnockback()
    {
        canBeKnocked = true;
    }

    protected void Move()
    {
        if (canMove)
        {
            rb.velocity = new Vector2(moveSpeed * movingInput, rb.velocity.y);
        }
    }

    protected void WallJump()
    {
        canMove = false;
        rb.velocity = new Vector2(wallJumpDirection.x * -facingDirection, wallJumpDirection.y);
    }

    protected void Jump()
    {
        rb.velocity = new Vector2(rb.velocity.x, jumpForce);
    }

    public void Push(float pushForce)
    {
        rb.velocity = new Vector2(rb.velocity.x, pushForce);
    }

    protected void FlipController()
    {
        if (facingRight && rb.velocity.x < -.1f)
        {
            Flip();
        }
        else if (!facingRight && rb.velocity.x > .1f)
        {
            Flip();
        }
    }

    protected void Flip()
    {
        facingDirection = facingDirection * -1;
        facingRight = !facingRight;
        transform.Rotate(0, 180, 0);
    }

    protected void CollisionChecks()
    {
        isGrounded = Physics2D.Raycast(transform.position, Vector2.down, groundCheckDistance, whatIsGround);
        //Leo tường
        isWallDetected = Physics2D.Raycast(transform.position, Vector2.right * facingDirection, wallCheckDistance, whatIsWall);
        
        if (isWallDetected && rb.velocity.y < 0)
        {
            canWallSlide = true;
        }
        
        if (!isWallDetected)
        {
            isWallSliding = false;
            canWallSlide = false;
        }
    }

    protected void OnDrawGizmos()
    {
        Gizmos.DrawLine(transform.position, new Vector3(transform.position.x + wallCheckDistance * facingDirection, transform.position.y));
        Gizmos.DrawLine(transform.position, new Vector3(transform.position.x, transform.position.y - groundCheckDistance));
        Gizmos.DrawWireSphere(enemyCheck.position, enemyCheckRadius);
    }
}
