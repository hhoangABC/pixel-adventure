using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    public static PlayerManager instance;

    public int fruits;
    public Transform respawnPoint;
    public GameObject currentPlayer;
    public int choosenSkinId;

    [SerializeField] protected GameObject playerPrefab;

    [Header("Camera Shake FX")]
    [SerializeField] protected CinemachineImpulseSource impulse;
    [SerializeField] protected Vector3 shakeDirection;
    [SerializeField] protected float forceMultiplier;

    public void ScreenShake(int facingDir)
    {
        impulse.m_DefaultVelocity = new Vector3(shakeDirection.x * facingDir, shakeDirection.y) * forceMultiplier;
        impulse.GenerateImpulse();
    }

    protected void Awake()
    {
        DontDestroyOnLoad(this.gameObject);

        if (instance == null) instance = this;
        else Destroy(this.gameObject);
        //PlayerRespawn();
    }

    protected void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            PlayerRespawn();
        }
    }

    public void PlayerRespawn()
    {
        if (currentPlayer == null)
        {
            currentPlayer = Instantiate(playerPrefab, respawnPoint.position, transform.rotation);
        }
        //else
        //{
        //    currentPlayer.transform.position = respawnPoint.position;
        //    currentPlayer.SetActive(true);
        //}
    }
}
