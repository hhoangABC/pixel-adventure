﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum FruitType
{
    apple,
    banana,
    cherry,
    kiwi,
    melon,
    orange,
    pineapple,
    strawberry
}

public class Fruit : MonoBehaviour
{
    protected Animator anim;
    [SerializeField] protected SpriteRenderer sr;
    public FruitType myFruitType;
    [SerializeField] protected Sprite[] fruitImage;

    protected void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<Player>() != null)
        {
            collision.GetComponent<Player>().fruits++;
            Destroy(gameObject);
        }
    }

    public void FruitSetup(int fruitIndex)
    {
        anim = GetComponent<Animator>();
        for (int i = 0; i < anim.layerCount; i++)
        {
            anim.SetLayerWeight(i, 0);
        }
        anim.SetLayerWeight(fruitIndex, 1);

        //random
        //myFruitType = (FruitType)Random.Range(0, System.Enum.GetValues(typeof(FruitType)).Length);
        //sr.sprite = fruitImage[((int)myFruitType)];
    }
}