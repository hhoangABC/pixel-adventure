using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FruitManager : MonoBehaviour
{
    [SerializeField] protected Transform[] fruitPosition;
    [SerializeField] protected GameObject fruitPrefab;
    [SerializeField] private bool randomFruits;

    protected int fruitIndex;

    protected void Start()
    {
        fruitPosition = GetComponentsInChildren<Transform>();
        int levelNumber = GameManager.instance.levelNumber;

        for (int i = 1; i < fruitPosition.Length; i++)
        {
            GameObject newFruit = Instantiate(fruitPrefab, fruitPosition[i]);

            Fruit fruitComponent = newFruit.GetComponent<Fruit>();

            if (randomFruits)
            {
                fruitIndex = UnityEngine.Random.Range(0, Enum.GetNames(typeof(FruitType)).Length);
                fruitComponent.FruitSetup(fruitIndex);
            }
            else
            {
                fruitComponent.FruitSetup(fruitIndex);
                fruitIndex++;

                if (fruitIndex > Enum.GetNames(typeof(FruitType)).Length)
                    fruitIndex = 0;
            }

            fruitPosition[i].GetComponent<SpriteRenderer>().sprite = null;
        }

        int totalAmountOffFruits = PlayerPrefs.GetInt("Level" + levelNumber + "TotalFruits");

        if (totalAmountOffFruits != fruitPosition.Length - 1)
        {
            PlayerPrefs.SetInt("Level" + levelNumber + "TotalFruits", fruitPosition.Length - 1);
        }
    }
}
