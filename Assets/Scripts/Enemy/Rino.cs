using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rino : Enemy
{
    [Header("Rino spesific")]
    [SerializeField] protected float agroSpeed = 8f;
    [SerializeField] protected float shockTime;
    protected float shockTimeCounter;

    protected override void Start()
    {
        base.Start();
        invincible = true;
    }

    void Update()
    {
        CollisionChecks();
        AnimationControllers();

        if (!playerDetection)
        {
            WalkAround();
            return;
        }

        if (playerDetection.collider != null && playerDetection.collider.GetComponent<Player>() != null)
        {
            aggresive = true;
        }

        if (!aggresive)
        {
            WalkAround();
        }
        else
        {
            if (!groundDetected)
            {
                aggresive = false;
                Flip();
            }

            rb.velocity = new Vector2(agroSpeed * facingDirection, rb.velocity.y);

            if (wallDetected && invincible)
            {
                invincible = false;
                shockTimeCounter = shockTime;
            }

            if (shockTimeCounter <= 0 && !invincible)
            {
                invincible = true;
                Flip();
                aggresive = false;
            }
            shockTimeCounter -= Time.deltaTime;
        }
    }

    protected void AnimationControllers()
    {
        anim.SetBool("invincible", invincible);
        anim.SetFloat("xVelocity", rb.velocity.x);
    }
}
