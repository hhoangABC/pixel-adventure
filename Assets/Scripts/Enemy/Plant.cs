using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plant : Enemy
{
    [Header("Plant spesific")]
    [SerializeField] protected GameObject bulletPrefab;
    [SerializeField] protected Transform bulletOrigin;
    [SerializeField] protected float bulletSpeed;
    [SerializeField] protected bool facingRight;

    protected override void Start()
    {
        base.Start();
        if (facingRight) Flip(); 
    }

    protected void Update()
    {
        CollisionChecks();
        idleTimeCounter -= Time.deltaTime;

        bool playerDetected = playerDetection.collider.GetComponent<Player>() != null;

        if (idleTimeCounter < 0 && playerDetected)
        {
            idleTimeCounter = idleTime;
            anim.SetTrigger("attack");
        }
    }

    protected void AttackEvent()
    {
        GameObject newBullet = Instantiate(bulletPrefab, bulletOrigin.transform.position, bulletOrigin.transform.rotation);
        newBullet.GetComponent<Bullet>().SetupSpeed(bulletSpeed * facingDirection, 0);
        Destroy(newBullet, 3f);
    }
}
