using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mushroom : Enemy
{
    protected override void Start()
    {
        base.Start();
    }

    protected void Update()
    {
        WalkAround();
        CollisionChecks();

        anim.SetFloat("xVelocity", rb.velocity.x);
    }
}
