using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trunk : Enemy
{
    [Header("Trunk spesific")]
    [SerializeField] protected float moveBackTime;
    protected float retreatTimeCounter;


    [Header("Collision spesific")]
    [SerializeField] protected float checkRadius;
    [SerializeField] protected LayerMask whatIsPlayer;
    [SerializeField] protected Transform groundBehindCheck;
    protected bool wallBehind;
    protected bool groundBehind;

    protected bool playerDetected;

    [Header("Bullet spesific")]
    [SerializeField] protected float attackCooldown;
    protected float attackCooldownCounter;

    [SerializeField] protected GameObject bulletPrefab;
    [SerializeField] protected Transform bulletOrigin;
    [SerializeField] protected float bulletSpeed;

    protected override void Start()
    {
        base.Start();
    }

    protected void Update()
    {
        CollisionChecks();

        if (!canMove)
        {
            rb.velocity = new Vector2 (0, 0);
        }

        attackCooldownCounter -= Time.deltaTime;
        retreatTimeCounter -= Time.deltaTime;

        if (playerDetected && retreatTimeCounter < 0)
        {
            retreatTimeCounter = moveBackTime;
        }

        if (playerDetection.collider.GetComponent<Player>() != null)
        {
            if (attackCooldownCounter < 0)
            {
                attackCooldownCounter = attackCooldown;
                anim.SetTrigger("attack");
                canMove = false;
            }
            else if (playerDetection.distance < 3)
            {
                MoveBackwards(1.5f);
            }
        }
        else
        {
            if (retreatTimeCounter > 0) MoveBackwards(4f);
            else WalkAround();
        }

        anim.SetFloat("xVelocity", rb.velocity.x);
    }

    protected void MoveBackwards(float multiplier)
    {
        if (wallBehind) return;

        if (!groundBehind) return;

        rb.velocity = new Vector2 (speed * multiplier * -facingDirection, rb.velocity.y);
    }

    protected void AttackEvent()
    {
        GameObject newBullet = Instantiate(bulletPrefab, bulletOrigin.transform.position, bulletOrigin.transform.rotation);
        newBullet.GetComponent<Bullet>().SetupSpeed(bulletSpeed * facingDirection, 0);
        Destroy(newBullet, 2.5f);
    }

    protected void ReturnMovement()
    {
        canMove = true;
    }

    protected override void CollisionChecks()
    {
        base.CollisionChecks();

        playerDetected = Physics2D.OverlapCircle(transform.position, checkRadius, whatIsPlayer);
        groundBehind = Physics2D.Raycast(groundBehindCheck.position, Vector2.down, groundCheckDistance, whatIsGround);
        wallBehind = Physics2D.Raycast(wallCheck.position, Vector2.right * (-facingDirection + 1), wallCheckDistance, whatIsGround);

    }

    protected override void OnDrawGizmos()
    {
        base.OnDrawGizmos();

        Gizmos.DrawWireSphere(transform.position, checkRadius);
        Gizmos.DrawLine(groundBehindCheck.position, new Vector2(groundBehindCheck.position.x, groundBehindCheck.position.y - groundCheckDistance));
    }
}
