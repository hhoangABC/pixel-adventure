using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bee : Enemy
{
    [Header("Bee spesific")]
    [SerializeField] protected Transform[] idlePoint;
    [SerializeField] protected float checkRadius;
    [SerializeField] protected LayerMask whatIsPlayer;
    [SerializeField] protected Transform playerCheck;
    [SerializeField] protected float yOffset;
    [SerializeField] protected float aggroSpeed;

    protected bool playerDetected;
    protected int idlePointIndex;
    protected float defaultSpeed;

    [Header("Bullet spesific")]
    [SerializeField] protected GameObject bulletPrefab;
    [SerializeField] protected Transform bulletOrigin;
    [SerializeField] protected float bulletSpeed;

    protected override void Start()
    {
        base.Start();
        defaultSpeed = speed;
    }

    protected void Update()
    {
        bool idle = idleTimeCounter > 0;

        anim.SetBool("idle", idle);
        idleTimeCounter -= Time.deltaTime;

        if (idle) return;

        if (player == null) return;

        playerDetected = Physics2D.OverlapCircle(playerCheck.position, checkRadius, whatIsPlayer);

        if (playerDetected && !aggresive)
        {
            aggresive = true;
            speed = aggroSpeed;
        }

        if (!aggresive)
        {
            transform.position = Vector2.MoveTowards(transform.position, idlePoint[idlePointIndex].position, speed * Time.deltaTime);

            if (Vector2.Distance(transform.position, idlePoint[idlePointIndex].position) < .1f)
            {
                idlePointIndex++;

                if (idlePointIndex >= idlePoint.Length) idlePointIndex = 0;
            }
        }
        else
        {
            Vector2 newPosition = new Vector2(player.transform.position.x, player.transform.position.y + yOffset);
            transform.position = Vector2.MoveTowards(transform.position, newPosition, speed * Time.deltaTime);

            float xDifference = transform.position.x - player.position.x;

            if (Mathf.Abs(xDifference) < .15f)
            {
                anim.SetTrigger("attack");
            }
        }
    }

    protected void AttackEvent()
    {
        GameObject newBullet = Instantiate(bulletPrefab, bulletOrigin.transform.position, bulletOrigin.transform.rotation);
        newBullet.GetComponent<Bullet>().SetupSpeed(0, -bulletSpeed);
        Destroy(newBullet, 2f);

        speed = defaultSpeed;
        idleTimeCounter = idleTime;
        aggresive = false;
    }

    protected override void OnDrawGizmos()
    {
        base.OnDrawGizmos();
        Gizmos.DrawWireSphere(playerCheck.position, checkRadius);
    }
}
