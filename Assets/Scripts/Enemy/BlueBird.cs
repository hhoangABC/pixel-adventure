using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlueBird : Enemy
{
    protected RaycastHit2D cellingDetected;

    [Header("Blue bird specific")]
    [SerializeField] protected float ceillingDistance;
    [SerializeField] protected float groundDistance;

    [SerializeField] protected float flyUpForce;
    [SerializeField] protected float flyDownForce;
    protected float flyForce;

    protected bool canFly = true;

    protected override void Start()
    {
        base.Start();
        flyForce = flyUpForce;
    }

    protected void Update()
    {
        CollisionChecks();

        if (cellingDetected) flyForce = flyDownForce;
        else if (groundDetected) flyForce = flyUpForce;

        if (wallDetected) Flip();
    }

    public override void Damage()
    {
        canFly = false;
        rb.velocity = new Vector2 (0, 0);
        rb.gravityScale = 0f;
        base.Damage();
    }

    public void FlyUpEvent()
    {
        if (canFly)
        {
            rb.velocity = new Vector2(speed * facingDirection, flyForce);
        }
    }

    protected override void CollisionChecks()
    {
        base.CollisionChecks();

        cellingDetected = Physics2D.Raycast(transform.position, Vector2.up, ceillingDistance, whatIsGround);
    }

    protected override void OnDrawGizmos()
    {
        base.OnDrawGizmos();

        Gizmos.DrawLine(transform.position, new Vector2(transform.position.x, transform.position.y + ceillingDistance));
        Gizmos.DrawLine(transform.position, new Vector2(transform.position.x, transform.position.y - groundDistance));
    }
}
