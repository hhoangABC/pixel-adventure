using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class InGame_UI : MonoBehaviour
{
    

    protected bool gamePaused;

    [Header("Menu gameobject")]
    [SerializeField] protected GameObject inGameUI;
    [SerializeField] protected GameObject pauseUI;
    [SerializeField] protected GameObject endLevelUI;

    [Header("TextComponents")]
    [SerializeField] protected TextMeshProUGUI timerText;
    [SerializeField] private TextMeshProUGUI currenFruitAmount;

    [SerializeField] protected TextMeshProUGUI endTimeText;
    [SerializeField] protected TextMeshProUGUI endBestTimeText;
    [SerializeField] protected TextMeshProUGUI endFruitsText;

    protected void Start()
    {
        GameManager.instance.levelNumber = SceneManager.GetActiveScene().buildIndex;

        Time.timeScale = 1;
        SwitchUI(inGameUI);
    }

    protected void Update()
    {
        UpdateInGameInfo();

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            CheckIfNotPause();
        }
    }

    protected bool CheckIfNotPause()
    {
        if (!gamePaused)
        {
            gamePaused = true;
            Time.timeScale = 0;
            SwitchUI(pauseUI);
            return true;
        }
        else
        {
            gamePaused = false;
            Time.timeScale = 1;
            SwitchUI(inGameUI);
            return false;
        }
    }

    public void OnLevelFinished()
    {
        endFruitsText.text = "Fruits: " + PlayerManager.instance.fruits;
        endTimeText.text = "Your time: " + GameManager.instance.timer.ToString("00") + " s";
        endBestTimeText.text = "Best time: " + PlayerPrefs.GetFloat("Level" + GameManager.instance.levelNumber + "BestTime", 999).ToString("00") + " s";

        SwitchUI(endLevelUI);
    }

    private void UpdateInGameInfo()
    {
        timerText.text = "Timer: " + GameManager.instance.timer.ToString("00") + " s";
        currenFruitAmount.text = PlayerManager.instance.fruits.ToString();
    }

    public void SwitchUI(GameObject uiMenu)
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(false);
        }

        uiMenu.SetActive(true);
    }

    public void LoadMainMenu()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene("MainMenu");
    }

    public void ReloadCurrentLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void LoadNextLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
}
