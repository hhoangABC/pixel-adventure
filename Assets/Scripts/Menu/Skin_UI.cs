using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Skin_UI : MonoBehaviour
{
    [SerializeField] protected int[] priceForSkin;

    [SerializeField] protected bool[] skinPurchased;
    
    protected int skin_Id;

    [Header("Components")]
    [SerializeField] protected GameObject buyButton;
    [SerializeField] protected GameObject selectButton;
    [SerializeField] protected Animator anim;
    [SerializeField] protected TextMeshProUGUI bankText;

    protected void SetupSkinInfo()
    {
        skinPurchased[0] = true;

        for (int i = 1; i < skinPurchased.Length; i++)
        {
            bool skinUnlocked = PlayerPrefs.GetInt("SkinPurchased" + i) == 1;

            if (skinUnlocked)
            {
                skinPurchased[i] = true;
            }
        }

        bankText.text = PlayerPrefs.GetInt("TotalFruitsCollected").ToString();

        selectButton.SetActive(skinPurchased[skin_Id]);
        buyButton.SetActive(!skinPurchased[skin_Id]);


        if (!skinPurchased[skin_Id])
        {
            buyButton.GetComponentInChildren<TextMeshProUGUI>().text = "Price: " + priceForSkin[skin_Id].ToString();
        }
        anim.SetInteger("skinId", skin_Id);
    }

    public bool EnoughMoney()
    {
        int totalFruits = PlayerPrefs.GetInt("TotalFruitsCollected");

        if (totalFruits > priceForSkin[skin_Id])
        {
            totalFruits = totalFruits - priceForSkin[skin_Id];

            PlayerPrefs.SetInt("TotalFruitsCollected", totalFruits);

            return true;
        }
        return false;
    }

    public void NextSkin()
    {
        skin_Id++;

        if (skin_Id > 3) skin_Id = 0;
        SetupSkinInfo();
    }

    public void PreviousSkin()
    {
        skin_Id--;

        if (skin_Id < 0) skin_Id = 3;
        SetupSkinInfo();
    }

    public void Buy()
    {
        if (EnoughMoney())
        {
            PlayerPrefs.SetInt("SkinPurchased" + skin_Id, 1);
            SetupSkinInfo();
        }
        else
        {
            Debug.Log("No money");
        }
    }

    public void Select()
    {
        if (PlayerManager.instance != null)
        {
            PlayerManager.instance.choosenSkinId = skin_Id;
        }
    }

    public void SwitchSelectButton(GameObject newButton)
    {
        selectButton = newButton;
    }

    protected void OnEnable()
    {
        SetupSkinInfo();
    }

    protected void OnDisable()
    {
        selectButton.SetActive(false);
    }
}
