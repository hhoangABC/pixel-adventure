using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class LevelButton_UI : MonoBehaviour
{
    [SerializeField] protected TextMeshProUGUI levelName;
    [SerializeField] protected TextMeshProUGUI bestTime;
    [SerializeField] protected TextMeshProUGUI collectedFruits;
    [SerializeField] protected TextMeshProUGUI totalFruits;

    public void UpdateTextInfo(int levelNumber)
    {
        levelName.text = "Level " + levelNumber;
        bestTime.text = "Best time: " + PlayerPrefs.GetFloat("Level" + levelNumber + "BestTime", 999).ToString("00" + " s");
        collectedFruits.text = PlayerPrefs.GetInt("Level" + levelNumber + "FruitsCollected", PlayerManager.instance.fruits).ToString();
        totalFruits.text = "/ " + PlayerPrefs.GetInt("Level" + levelNumber + "TotalFruits");
    }

}
