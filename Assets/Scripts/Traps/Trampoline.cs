using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trampoline : MonoBehaviour
{
    [SerializeField] protected float pushForce;
    protected bool canBeUse = true;

    protected void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<Player>() != null && canBeUse)
        {
            canBeUse = false;
            GetComponent<Animator>().SetTrigger("activate");
            collision.GetComponent<Player>().Push(pushForce);
        }
    }

    protected void CanUseAgain() => canBeUse = true;
}
