﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Saw : Danger
{
    protected Animator anim;
    //protected bool isWorking;

    [SerializeField] protected Transform[] movePoint;
    [SerializeField] protected float speed = 4f;
    [SerializeField] protected float cooldown = 1f;

    protected int movePointIndex;
    protected float cooldownTimer;

    protected void Start()
    {
        anim = GetComponent<Animator>();
        transform.position = movePoint[0].position;
    }

    protected void Update()
    {
        cooldownTimer -= Time.deltaTime;

        bool isWorking = cooldownTimer < 0;
        anim.SetBool("isWorking", isWorking);

        if (isWorking )
        {
            transform.position = Vector3.MoveTowards(transform.position, movePoint[movePointIndex].position, speed * Time.deltaTime);
        }

        if (Vector2.Distance(transform.position, movePoint[movePointIndex].position) < 0.15f)
        {
            Flip();
            cooldownTimer = cooldown;
            movePointIndex++;

            if (movePointIndex >= movePoint.Length)
            {
                movePointIndex = 0;
            }
        }
    }

    protected void Flip()
    {
        //Quay hướng Saw
        transform.localScale = new Vector3(1, transform.localScale.y * -1);
    }
}
