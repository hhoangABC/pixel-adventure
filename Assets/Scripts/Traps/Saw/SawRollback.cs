﻿using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class SawRollback : Danger
{
    protected Animator anim;

    [SerializeField] protected Transform[] movePoint;
    [SerializeField] protected float speed = 4f;

    protected int movePointIndex;
    protected bool goingForward = true;

    protected void Start()
    {
        anim = GetComponent<Animator>();
        anim.SetBool("isWorking", true);
        transform.position = movePoint[0].position;

        Flip();
    }

    protected void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, movePoint[movePointIndex].position, speed * Time.deltaTime);

        if (Vector2.Distance(transform.position, movePoint[movePointIndex].position) < 0.15f)
        {
            if (movePointIndex == 0)
            {
                Flip();
                goingForward = true;
            }

            if (goingForward)
            {
                movePointIndex++;
            }
            else
            {
                movePointIndex--;
            }

            if (movePointIndex >= movePoint.Length)
            {
                movePointIndex = movePoint.Length - 1;
                goingForward = false;
                Flip();
            }
        }
    }

    protected void Flip()
    {
        transform.localScale = new Vector3(1, transform.localScale.y * -1);
    }
}
