using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Spiked_Ball : MonoBehaviour
{
    [SerializeField] protected Rigidbody2D rb;
    [SerializeField] protected Vector2 pushDirection;

    protected void Start()
    {
        rb.AddForce(pushDirection, ForceMode2D.Impulse);
    }
}
