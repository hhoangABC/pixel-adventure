using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire_Switcher : MonoBehaviour
{
    protected Fire myTrap;
    protected Animator anim;

    [SerializeField] protected float timeNoActive = 2f;

    protected void Start()
    {
        anim = GetComponent<Animator>();
        myTrap = GetComponentInChildren<Fire>();
    }

    protected void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<Player>() != null)
        {
            anim.SetTrigger("pressed");
            myTrap.FireSwitchAfter(timeNoActive);
        }
    }
}
