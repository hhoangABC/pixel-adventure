using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fire : Danger
{
    public bool isWoking;
    protected Animator anim;

    public float repeatRate;

    protected void Start()
    {
        anim = GetComponent<Animator>();

        if (transform.parent == null)
        {
            InvokeRepeating("FireSwitch", 0, repeatRate);
        }
    }

    protected void Update()
    {
        anim.SetBool("isWoking", isWoking);
    }

    public void FireSwitch()
    {
        isWoking = !isWoking;
    }

    public void FireSwitchAfter(float seconds)
    {
        CancelInvoke();
        isWoking = false;
        Invoke("FireSwitch", seconds);
    }

    protected override void OnTriggerEnter2D(Collider2D collision)
    {
        if (isWoking)
        {
            base.OnTriggerEnter2D(collision);
        }
    }
}
