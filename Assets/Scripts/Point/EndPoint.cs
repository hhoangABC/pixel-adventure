using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndPoint : MonoBehaviour
{
    protected InGame_UI inGame_UI;

    protected void Start()
    {
        inGame_UI = GameObject.Find("Canvas").GetComponent<InGame_UI>();
    }

    protected void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.GetComponent<Player>() != null)
        {
            GetComponent<Animator>().SetTrigger("activate");

            Destroy(collision.gameObject);

            inGame_UI.OnLevelFinished();

            GameManager.instance.SaveCollectedFruits();
            GameManager.instance.SaveBestTime();
            GameManager.instance.SaveLevelInfo();
        }
    }
}
